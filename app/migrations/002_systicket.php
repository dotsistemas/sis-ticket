<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of 002_systicket
 *
 * @author elieldepaula
 */
class Migration_Systicket extends CI_Migration
{
    /**
     * Common fields to the tables.
     * @var mixed 
     */
    protected $common_fields = array(
        'created_on' => array(
            'type' => 'datetime',
            'null' => TRUE,
        ),
        'modified_on' => array(
            'type' => 'datetime',
            'null' => TRUE,
        ),
        'created_by' => array(
            'type' => 'int',
            'constraint' => 11,
            'default' => 0
        ),
        'modified_by' => array(
            'type' => 'int',
            'constraint' => 11,
            'default' => 0
        ),
        'deleted' => array(
            'type' => 'int',
            'constraint' => 11,
            'default' => 0
        ),
        'deleted_by' => array(
            'type' => 'int',
            'constraint' => 11,
            'default' => 0
        ),
    );
    
    /**
     * Fields of departments table.
     * 
     * @var Array
     */
    protected $fields_departments = array(
        'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
        ),
        'account_id' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => FALSE
        ),
        'title' => array(
                'type' => 'varchar',
                'constraint' => 45,
                'null' => TRUE
        ),
        'description' => array(
                'type' => 'text',
                'null' => TRUE
        ),
        'manager' => array(
                'type' => 'varchar',
                'constraint' => 60,
                'null' => TRUE
        ),
        'email' => array(
                'type' => 'varchar',
                'constraint' => 100,
                'null' => TRUE
        ),
        'status' => array(
                'type' => 'int',
                'null' => FALSE,
                'default' => '0'
        )
    );
    
    /**
     * Fields of tickets table.
     * 
     * @var array
     */
    protected $fields_tickets = array(
        'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
        ),
        'department_id' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => FALSE
        ),
        'account_id' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => FALSE
        ),
        'subject' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => TRUE
        ),
        'content' => array(
                'type' => 'text',
                'null' => TRUE
        ),
        'attachment' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => TRUE
        ),
        'priority' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => TRUE
        ),
        'status' => array(
                'type' => 'int',
                'null' => FALSE,
                'default' => '0'
        )
    );
    
    /**
     * Fields of ticket_messages table.
     * 
     * @var array
     */
    protected $fields_ticket_messages = array(
        'id' => array(
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
        ),
        'ticket_id' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => FALSE
        ),
        'account_id' => array(
                'type' => 'int',
                'constraint' => 11,
                'null' => FALSE
        ),
        'content' => array(
                'type' => 'text',
                'null' => TRUE
        ),
        'attachment' => array(
                'type' => 'varchar',
                'constraint' => 255,
                'null' => TRUE
        )
    );


    public function up()
    {
        // Create departments table.
        $this->dbforge->add_field($this->fields_departments);
        $this->dbforge->add_field($this->common_fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('account_id');
        $this->dbforge->create_table('departments', true);
        // Create tickets table.
        $this->dbforge->add_field($this->fields_tickets);
        $this->dbforge->add_field($this->common_fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('account_id');
        $this->dbforge->add_key('department_id');
        $this->dbforge->create_table('tickets', true);
        // Create ticket_messages table.
        $this->dbforge->add_field($this->fields_ticket_messages);
        $this->dbforge->add_field($this->common_fields);
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('account_id');
        $this->dbforge->add_key('ticket_id');
        $this->dbforge->create_table('ticket_messages', true);
    }
    
    public function down()
    {
        // Delete ticket_messages table.
        $this->dbforge->drop_table('ticket_messages');
        // Delete tickets table.
        $this->dbforge->drop_table('tickets');
        // Delete departments table.
        $this->dbforge->drop_table('departments');
    }
}
