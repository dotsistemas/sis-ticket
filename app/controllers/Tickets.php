<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta classe contém os principais do sistema de tickets.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 * @since v2.0.0
 */
class Tickets extends MY_Controller
{

    function __construct()
    {

        $this->model_file = array('ticket', 'ticketmessage', 'department');

        parent::__construct();
        $this->wpanel->check_setup();

        if ($this->auth->is_logged() and $this->auth->is_admin())
        {
            $this->set_message('Esta área é destinada somente a usuários comuns.', 'info');
            $this->auth->logout();
        }
    }

    /**
     * Listagem dos tickets de chamado.
     */
    public function index()
    {
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        $this->table->set_heading('#', 'Título', 'Departamento', 'Data', 'Respostas', 'Status');
        $query = $this->ticket->select('tickets.*')->order_by('created_on', 'desc')->find_many_by('tickets.created_by', $this->auth->user_id());
        $status = config_item('status_tickets');
        foreach ($query as $row)
        {
            $this->table->add_row(
                    anchor('tickets/view/' . $row->id, str_pad($row->id, 5, '0', STR_PAD_LEFT)), $row->subject, $this->department->departament_name($row->department_id), '<small>' . datetime_for_user($row->created_on, false) . '</small>', $this->ticketmessage->count_by('ticket_id', $row->id), $status[$row->status]
            );
        }
        if(count($query) <= 0)
            $this->set_var('tickets', '<p class="text-danger">Nenhum chamado para listar.</p>');
        else
            $this->set_var('tickets', $this->table->generate());
        $this->render();
    }

    /**
     * Formulário de abertura de tickets de chamados.
     */
    public function create()
    {
        $this->form_validation->set_rules('subject', 'Título', 'required');
        $this->form_validation->set_rules('content', 'Mensagem', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $query_depto = $this->department->find_many_by('status', 1);
            $this->set_var('departamentos', $query_depto);
            $this->render();
        } else
        {
            $data = array(
                'account_id' => $this->auth->user_id(),
                'department_id' => $this->input->post('department_id'),
                'subject' => $this->input->post('subject'),
                'content' => $this->input->post('content'),
                'attachment' => $this->wpanel->upload_media('anexos'),
                'priority' => $this->input->post('priority'),
                'status' => 0,
            );
            // Salva o ticket.
            if ($this->ticket->insert($data))
            {
                // Envia o email de notificação.
                //--------------------------------------------------------------
                $data_message = array(
                    'ticket_user' => auth_extra_data('name'),
                    'ticket_email' => auth_login_data('email'),
                    'department_name' => $this->department->departament_name($data['department_id']),
                    'ticket_title' => $data['subject'],
                    'ticket_created_on' => date('d/m/Y H:i:s'),
                    'ticket_ip' => $_SERVER['REMOTE_ADDR'],
                    'ticket_content' => $data['content']
                );
                $data_email = array(
                    'html' => TRUE,
                    'from_name' => wpn_config('site_titulo'),
                    'from_email' => wpn_config('site_contato'),
                    'to' => $this->department->notification_contacts($data['department_id']),
                    'subject' => 'Novo chamado recebido.',
                    'message' => $this->load->view('emails/ticket_create', $data_message, TRUE),
                );
                $this->wpanel->send_email($data_email);
                //--------------------------------------------------------------
                $this->set_message('Chamado aberto com sucesso!', 'success', 'tickets');
            } else
                $this->set_message('Seu chamado não pode ser aberto, verifique seus dados e tente novamente.', 'danger', 'tickets/create');
        }
    }

    /**
     * Encerra um ticket de chamado.
     * 
     * @param int $ticket_id Id do ticket de chamado.
     */
    public function close($ticket_id = NULL)
    {
        if ($ticket_id == NULL)
            $this->set_message('Selecione um ticket de chamado para ser visualizado.', 'danger', 'tickets');
        
        $data['status'] = 2;
        if ($this->ticket->update($ticket_id, $data))
            $this->set_message('Chamado encerrado com sucesso.', 'success', 'tickets/view/'.$ticket_id);
        else
            $this->set_message('Erro ao encerrar o chamado.', 'danger', 'tickets/view/'.$ticket_id);
    }

    /**
     * Visualiza os detalhes do ticket de chamado.
     *
     * @param int $ticket_id Id do ticket de chamado.
     */
    public function view($ticket_id = NULL)
    {
        if ($ticket_id == NULL)
            $this->set_message('Selecione um ticket de chamado para ser visualizado.', 'danger', 'tickets');
        $this->wpanel->set_meta_title('Acompanhamento de chamados');
        $query_ticket = $this->ticket->find($ticket_id);
        $this->form_validation->set_rules('message', 'Mensagem', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $query_message = $this->ticketmessage->find_many_by('ticket_id', $ticket_id);
            $html = "";
            foreach ($query_message as $msg)
            {
                $account = $this->auth->account($msg->account_id);
                $extra = (object) json_decode($account->extra_data);
                $nome = $extra->name;
                $avatar = $extra->avatar;
                $img = array(
                    'src' => 'media/avatar/' . $avatar,
                    'width' => '64',
                    'height' => '64'
                );
                $html .= '<li class="media">';
                if ($avatar)
                    $html .= '    <a class="pull-left" href="#">' . img($img) . '</a>';
                else
                    $html .= '<a class="pull-left" href="#"><img class="media-object" src="' . base_url('assets/img/no-user.jpg') . '" height="64" width="64"></a>';
                $html .= '    <div class="media-body">';
                $html .= '      <h4 class="media-heading">' . $nome . '</h4>';
                $html .= '      <p>' . $msg->content . '</p>';
                if ($msg->attachment)
                {
                    $html .= '      <p><a href="' . base_url('./media/anexos/' . $msg->attachment) . '" target="_blank" class="btn btn-xs btn-primary">Anexo</a></p>';
                }
                $html .= '      <p class="text-muted">' . datetime_for_user($msg->created_on) . '</p>';
                $html .= '  </div>';
                $html .= '</li>';
            }
            $this->set_var('messages', $html);
            $this->set_var('status', config_item('status_tickets'));
            $this->set_var('ticket', $query_ticket);
            $this->render();
        } else
        {
            $data = array(
                'ticket_id' => $ticket_id,
                'account_id' => $this->auth->user_id(),
                'content' => $this->input->post('message'),
                'attachment' => ''
            );
            if ($this->ticketmessage->insert($data))
            {
                // Envia o email de notificação.
                //--------------------------------------------------------------
                $data_message = array(
                    'ticket_user' => auth_extra_data('name'),
                    'ticket_email' => auth_login_data('email'),
                    'ticket_title' => 'Nova resposta ao chamado #' . str_pad($ticket_id, 5, '0', STR_PAD_LEFT),
                    'ticket_created_on' => date('d/m/Y H:i:s'),
                    'ticket_ip' => $_SERVER['REMOTE_ADDR'],
                    'ticket_content' => $data['content']
                );
                $data_email = array(
                    'html' => TRUE,
                    'from_name' => wpn_config('site_titulo'),
                    'from_email' => wpn_config('site_contato'),
                    'to' => $this->department->notification_contacts($query_ticket->department_id) . ',' . auth_login_data('email'),
                    'subject' => 'Nova resposta ao chamado #' . str_pad($ticket_id, 5, '0', STR_PAD_LEFT),
                    'message' => $this->load->view('emails/ticket_message', $data_message, TRUE),
                );
                $this->wpanel->send_email($data_email);
                //--------------------------------------------------------------
                $this->set_message('Resposta enviada com sucesso.', 'success', 'tickets/view/' . $ticket_id);
            } else
                $this->set_message('Não foi possível criar a mensagem do chamado.', 'danger', 'tickets/view/' . $ticket_id);
        }
    }

}
