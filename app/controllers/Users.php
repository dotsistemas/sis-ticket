<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This class have the basic methods to manage an public user area into an website.
 * 
 * @package     WpanelCms
 * @subpackage  Controllers
 * @category    Controllers
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @extends     MY_Controller
 * @since       v1.0.0
 */
class Users extends MY_Controller
{

    /**
     * Class constructor.
     *
     * @return void
     */
    function __construct()
    {
        
        $this->model_file = array('ticket', 'ticketmessage', 'department');

        parent::__construct();
        $this->wpanel->check_setup();

        if ($this->auth->is_logged() and $this->auth->is_admin())
        {
            $this->set_message('Esta área é destinada somente a usuários comuns.', 'info', '');
            $this->auth->logout();
        }
    }

    /**
     * User dashboard.
     * 
     * @return void
     */
    public function index()
    {
        // Check the login.
        if (!$this->auth->is_logged())
            redirect('users/login');
        $this->wpanel->set_meta_title('Área de usuários');
        
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        $this->table->set_heading('#', 'Título', 'Departamento', 'Data', 'Respostas', 'Status');
        $query = $this->ticket->select('tickets.*')->where('tickets.status !=', 2)->order_by('created_on', 'desc')->find_many_by('tickets.created_by', $this->auth->user_id());
        $status = config_item('status_tickets');
        foreach ($query as $row)
        {
            $this->table->add_row(
                    anchor('tickets/view/' . $row->id, str_pad($row->id, 5, '0', STR_PAD_LEFT)), $row->subject, $this->department->departament_name($row->department_id), '<small>' . datetime_for_user($row->created_on, false) . '</small>', $this->ticketmessage->count_by('ticket_id', $row->id), $status[$row->status]
            );
        }
        if(count($query) <= 0)
            $this->set_var('tickets', '<p class="text-danger">Nenhum chamado para listar.</p>');
        else
            $this->set_var('tickets', $this->table->generate());
        
        $this->render();
    }

    /**
     * This is an stant-alone account register page.
     * 
     * @return  void
     */
    public function register()
    {
        $this->form_validation->set_rules('name', 'Nome', 'required');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        $this->form_validation->set_rules('password', 'Senha', 'required');
        $this->form_validation->set_rules('confpass', 'Confirmação de senha', 'required|matches[password]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->wpanel->set_meta_title('Cadastro de usuários');
            $this->render();
        } else
        {
            $extra_data = array(
                'name' => $this->input->post('name'),
                'avatar' => '',
                'skin' => ''
            );
            if ($this->auth->register($this->input->post('email'), $this->input->post('password'), 'user', $extra_data))
                redirect('users/registerok');
            else
                $this->set_message('Sua conta não pode ser criada, verifique seus dados e tente novamente.', 'danger', 'users/register');
        }
    }

    /**
     * Simple register success information.
     * 
     * @return void
     */
    public function registerok()
    {
        $this->wpanel->set_meta_title('Cadastro de usuários');
        $this->render();
    }

    /**
     * This method activate an account by token.
     * 
     * @author Eliel de Paula <dev@elieldepaula.com.br>
     * @param string $token
     */
    public function activate($token)
    {
        try
        {
            $this->auth->activate_token_account($token);
            $this->wpanel->set_meta_title('Ativação de usuários');
            $this->render();
        } catch (Exception $e)
        {
            //TODO Verificar forma de tratar o erro com o usuário.
            $this->set_message('Sua conta não pode ser ativada, verifique seus dados e tente novamente.', 'danger', 'users');
        }
    }

    /**
     * Recovery the password of the account.
     * 
     * @author Eliel de Paula <dev@elieldepaula.com.br>
     * @param string $token
     */
    public function recovery($token = NULL)
    {
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        if ($this->form_validation->run() == FALSE)
        {
            if ($token)
            {
                if ($this->auth->recovery($token))
                    $this->view('users/recovery_done')->render();
                else
                    $this->set_message('Link de confirmação inválido.', 'danger', 'users/recovery');
            } else
                $this->view('users/recovery_form')->render();
        } else
        {
            $email = $this->input->post('email');
            if ($this->auth->email_exists($email) == FALSE)
                $this->set_message('O e-mail informado não existe em nosso cadastro.', 'danger', 'users/recovery');

            if ($this->auth->send_recovery($email))
                $this->view('users/recovery_sent')->render();
            else
                $this->set_message('Houve um erro inesperado e sua senha não pode ser redefinida. Tente novamente mais tarde.', 'danger', 'users/recovery');
        }
    }

    /**
     * Profile user account page.
     * 
     * @author Eliel de Paula <dev@elieldepaula.com.br>
     * @return void
     */
    public function profile()
    {
        // Verifica o login.
        if (!$this->auth->is_logged())
            redirect('users/login');

        // Recupera os dados do usuário.
        $query = $this->auth->account();
        $profile = $this->auth->profile();

        $this->form_validation->set_rules('name', 'Nome', 'required');
        $this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
        if ($this->input->post('alt_password'))
            $this->form_validation->set_rules('password', 'Senha', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->wpanel->set_meta_title('Área de usuários');
            $this->set_var('account', $query);
            $this->set_var('profile', $profile);
            $this->render();
        } else
        {
            // Salva os dados adicionais na coluna 'extra_data' na tabela accounts.
            $profile->name = $this->input->post('name');
            $profile->demo = $this->input->post('demo');

            if ($this->auth->update($query->id, $this->input->post('email'), 'user', $profile))
            {
                // Verifica se deve alterar a senha do usuário.
                if ($this->input->post('alt_password'))
                    $this->auth->change_password($query->id, $this->input->post('password'));
                $this->set_message('Seus dados foram alterados com sucesso.', 'success', 'users/profile');
            } else
                $this->set_message('Houve um erro inesperado e seus dados não puderam ser alterados. Tente novamente mais tarde.', 'danger', 'users/profile');
        }
    }

    /**
     * Login and register page, also have links to login with facebook and G+.
     * 
     * @return mixed
     */
    public function login()
    {
        // Login pelo Google-Plus.
        if (isset($_GET['code']) && !isset($_GET['state']))
        {
            $this->googleplus->getAuthenticate();
            if ($this->auth->register_or_login_gplus($this->googleplus->getUserInfo()))
                redirect('users');
            else
                redirect('users/login');
        }
        // Login pelo Facebook.
        if (isset($_GET['code']) && isset($_GET['state']))
        {
            $user = $this->facebook->getUser();
            print_r($user);
            try
            {
                $data = $this->facebook->api('/me?fields=id,first_name,last_name,picture,email');
                if ($this->auth->register_or_login_facebook($data))
                    redirect('users');
                else
                    redirect('users/login');
            } catch (FacebookApiException $e)
            {
                print_r($e);
                $user = null;
            }
        }
        // Login pela biblioteca Auth() do WpanelCMS-Pro.
        $this->form_validation->set_rules('email', wpn_lang('input_email', 'Email'), 'required|valid_email');
        $this->form_validation->set_rules('password', wpn_lang('input_password', 'Password'), 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->wpanel->set_meta_title('Login de usuários');
//            $this->set_var('login_gplus', $this->googleplus->loginURL());
//            $this->set_var('login_facebook', $this->facebook->getLoginUrl(array(
//                        'redirect_uri' => base_url() . 'index.php/users/login',
//                        'scope' => array("email", "public_profile", "user_friends") // permissions here
//            )));
            $this->render();
        } else
        {
            if ($this->auth->login($this->input->post('email'), $this->input->post('password')))
                return redirect('users');
            else
                $this->set_message('Seu login falhou, tente novamente.', 'danger', 'users/login');
        }
    }
    
    /**
     * Altera a foto do aluno
     *
     * @return void
     */
    public function change_avatar()
    {
        $query = $this->auth->account();
        $profile = $this->auth->profile();
        $profile->avatar = $this->wpanel->upload_media('avatar');
        if ($this->auth->update($query->id, $query->email, 'user', $profile))
            $this->set_message('Sua foto foi alterada com sucesso!', 'success', 'users/profile');
        else
            $this->set_message('Houve um erro na alteração da sua foto, verifique e tente novamente.', 'danger', 'users/profile');
    }

    /**
     * Logout the user.
     * 
     * @return void
     */
    public function logout()
    {
//        $this->facebook->destroySession();
        $this->auth->logout();
        redirect();
    }

}
