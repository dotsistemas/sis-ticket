<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
 if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ticketmessage class.
 *
 * Gerado automaticamente pelo Wpanel GEN beta.
 * 
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 * @since v1.0.0
 */
class Ticketmessage extends MY_Model 
{

	/**
	 * Table name.
	 * @var string
	 */
	public $table_name = 'ticket_messages';

	/**
	 * Primary key field name.
	 * @var string
	 */
    public $primary_key = 'id';

    /**
	 * Datetiem format.
	 * @var string
	 */
    public $date_format = 'datetime';

    /**
	 * Soft delete enabled.
	 * @var boolean
	 */
    protected $soft_deletes = TRUE;

    /**
	 * Log user enabled.
	 * @var boolean
	 */
    protected $log_user = TRUE;

    /**
	 * Set created enabled.
	 * @var boolean
	 */
    protected $set_created = TRUE;

    /**
	 * Set modified enabled.
	 * @var boolean
	 */
    protected $set_modified = TRUE;

}

// End of file models/Ticketmessage.php