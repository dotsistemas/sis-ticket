<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?= $ticket_title; ?> | <?= wpn_config('site_titulo'); ?></title>
        <style>
            body {
                font-family: verdana;
                margin: 30px;
                background-color: #cccccc;
            }
            .content {
                padding: 30px;
                background-color: #ffffff;
            }
            .small {
                font-size: 11px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <h2><?= $ticket_title; ?> | <?= wpn_config('site_titulo')?> </h2>
            <p>
                <b>Autor:</b> <?= $ticket_user; ?><br/>
                <b>E-mail:</b> <?= $ticket_email; ?><br/>
                <b>Data:</b> <?= $ticket_created_on; ?><br/>
                <b>IP:</b> <?= $ticket_ip; ?><br/>
            </p>
            <p>
                <b>Conteúdo da mensagem:</b><br/>
                <?= $ticket_content; ?>
            </p>
            <hr/>
            <p class="small"><?= wpn_config('copyright'); ?></p>
        </div>
    </body>
</html>
