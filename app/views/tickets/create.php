<?= form_open_multipart('tickets/create', array('class' => 'form-horizontal', 'role' => 'form')) ?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <h1 class="page-header">Novo chamado</h1>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="department_id" class="col-sm-2 control-label">Departamento</label>
            <div class="col-sm-10">
                <select class="form-control" name="department_id" id="department_id">
                    <option value="0">[Selecione um departamento]</option>
                    <?php foreach ($departamentos as $dept)
                    { ?>
                        <option value="<?= $dept->id; ?>"><?= $dept->title; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id" class="col-sm-2 control-label">Título</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="subject" placeholder="Título do chamado">
                <?= form_error('subject'); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id" class="col-sm-2 control-label">Prioridade</label>
            <div class="col-sm-3">
                <select class="form-control" name="priority">
                    <option value="0">Baixa</option>
                    <option value="1">Média</option>
                    <option value="2">Alta</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id" class="col-sm-2 control-label">Mensagem</label>
            <div class="col-sm-10">
                <textarea class="form-control" name="content" placeholder="Texto do seu chamado"rows="5"></textarea>
                <?= form_error('content'); ?>
            </div>
        </div>
        <div class="form-group">
            <label for="department_id" class="col-sm-2 control-label">Anexar um arquivo</label>
            <div class="col-sm-10">
                <input type="file" name="userfile" class="form-control" placeholder="Título do chamado">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <?php echo anchor('tickets', '<i class="fa fa-ban"></i> Cancelar', array('class' => 'btn btn-danger')); ?>
        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Abrir o chamado</button>
    </div>
</div>
<?= form_close(); ?>