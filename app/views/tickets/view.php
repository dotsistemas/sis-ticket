<div class="row">
    <div class="col-md-12">
        <h1>[<?php echo str_pad($ticket->id, 5, '0', STR_PAD_LEFT); ?>] <?php echo $ticket->subject; ?></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <blockquote>
            <p><?php echo $ticket->content; ?><p>
        </blockquote>
        <span class="label label-default"><?php echo datetime_for_user($ticket->created_on); ?></span>
        <?php echo $status[$ticket->status]; ?>
        <?php if($ticket->attachment){ ?>
            <p>
                <b>Anexo: </b><br/>
                <a href="<?php echo base_url('./media/anexos/' . $ticket->attachment); ?>" target="_blank" class="btn btn-primary btn-xs">Baixar anexo</a>
            </p>
        <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Responder</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo form_open('tickets/view/' . $ticket->id, array('role' => 'form')); ?>
        <div class="form-group">
            <label class="control-label" for="message">Mensagem</label>
            <textarea class="form-control" name="message" id="message" placeholder="Digite sua resposta aqui." rows="5"></textarea>
            <?php echo form_error('message'); ?>
        </div>
        <button type="submit" class="btn btn-success"><span class="fa fa-comments-o"></span> Enviar a resposta</button>
        <?php if ($ticket->status != 2) echo anchor('tickets/close/' . $ticket->id, '<i class="fa fa-close"></i> Encerrar Chamado', array('class' => "btn btn-danger")); ?>
        <?php echo anchor('tickets', '<span class="fa fa-caret-left"></span> Voltar', array('class' => 'btn btn-primary pull-right')); ?>
        <?php echo form_close(); ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-md-12">
        <ul class="media-list">
            <?php echo $messages; ?>
        </ul>
    </div>
</div>