<h1 class="page-header">Tickets</h1>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <ul class="nav nav-pills">
            <li role="presentation"><?= anchor('users', 'Dashboard'); ?></li>
            <li role="presentation" class="active"><?= anchor('tickets', 'Tickets'); ?></li>
            <li role="presentation"><?= anchor('users/profile', 'Meus dados'); ?></li>
            <li role="presentation"><?= anchor('users/logout', 'Sair'); ?></li>
        </ul>
    </div>
</div>
<hr/>
<p><?php echo anchor('tickets/create', '<span class="fa fa-plus"></span> Novo ticket', array('class' => 'btn btn-xs btn-success')); ?></p>
<hr/>
<?php echo $tickets; ?>