<h1 class="page-header">Área do usuário</h1>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <ul class="nav nav-pills">
            <li role="presentation"><?= anchor('users', 'Dashboard'); ?></li>
            <li role="presentation"><?= anchor('tickets', 'Tickets'); ?></li>
            <li role="presentation" class="active"><?= anchor('users/profile', 'Meus dados'); ?></li>
            <li role="presentation"><?= anchor('users/logout', 'Sair'); ?></li>
        </ul>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <p>Use o formulário abaixo para atualiar seus dados.</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-6">
        <?= form_open('users/profile', array('role' => 'form', 'class' => 'form-horizontal',)); ?>
        <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Nome</label>
            <div class="col-sm-8">
                <input type="text" name="name" id="name" value="<?= $profile->name ?>" class="form-control"  />
                <?= form_error('name'); ?>
            </div>
        </div>
        <div class="form-group" id="fantasia">
            <label for="email" class="col-sm-4 control-label">E-mail</label>
            <div class="col-sm-8">
                <input type="text" name="email" id="email" value="<?= $account->email; ?>" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label for="senha" class="col-sm-4 control-label">Senha</label>
            <div class="col-sm-5">
                <input type="password" name="password" id="password" class="form-control" />
                <?= form_error('password'); ?>
                <label>
                    <input type="checkbox" name="alt_password" value="1"/> Alterar a senha.
                </label>
            </div>
        </div>
        <hr/>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-5">
                <button type="submit" class="btn btn-primary" >Salvar</button>
                <?= anchor('users', 'Cancelar', array('class' => 'btn btn-danger')); ?>
            </div>
        </div>
        <?= form_close(); ?>
    </div>
    <div class="col-sm-6 col-md-6">
        <div class="row">
            <div class="col-sm-5 col-md-5">
                <?php if ($profile->avatar) { ?>
                    <p><img src="<?= base_url('media/avatar') . '/' . $profile->avatar; ?>" class="img-responsive img-circle" /></p>
                <?php } else { ?>
                    <p><img src="<?= base_url('lib/img'); ?>/no-user.jpg" class="img-responsive img-circle" /></p>
                <?php } ?>
            </div>
            <div class="col-sm-7 col-md-7">
                <a href="#" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#avatar_modal">Alterar meu avatar</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="avatar_modal" tabindex="-1" role="dialog">
    <?php echo form_open_multipart('users/change_avatar'); ?>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Alterar meu avatar</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="avatar" class="control-label">Imagem</label>
                    <input type="file" name="userfile" id="avatar" class="form-control" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-success">Salvar</button>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>