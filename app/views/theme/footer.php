                </div>
                <div class="col-md-3 wpn-sidebar">
                    <h4 class="page-header">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span> Pesquisa
                    </h4>
                    <?= $this->widget->load('wpnsearchform'); ?>

                    <h4 class="page-header">
                        <span class="glyphicon glyphicon-tag" aria-hidden="true"></span> Base de Conhecimento
                    </h4>
                    <?= $this->widget->load('wpncategorymenu', array('inicial_id' => 0, 'main_attr' => array('class' => 'menu-sidebar'))); ?>

                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-4">
                    <h4 class="page-header">Sobre</h4>
                    <?= $this->widget->load('Wpnsitedescription'); ?>
                </div>
                <div class="col-md-4">
                    <h4 class="page-header">Newsletter</h4>
                    <?= $this->widget->load('Wpnformnewsletter'); ?>
                </div>
                <div class="col-md-4">
                    <h4 class="page-header">Social</h4>
                    <?= $this->widget->load('wpnlikebox'); ?>
                </div>
            </div> -->
            <div class="row wpn-footer">
                <div class="col-md-12 wpn-copyright">
                    <p><?= wpn_config('copyright'); ?></p>
                </div>
            </div>
        </div>
        <!-- jQuery -->
        <?= wpn_asset('js', 'jquery-2.1.4.min.js'); ?>
        <?= wpn_asset('js', 'bootstrap.min.js'); ?>
        <?= wpn_asset('js', 'bootstrap-submenu.min.js'); ?>
        <?= wpn_asset('js', 'ripples.min.js'); ?>
        <?= wpn_asset('js', 'material.min.js'); ?>
        <script src="//fezvrasta.github.io/snackbarjs/dist/snackbar.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>
        <script type="text/javascript">

        $(document).ready(function() {
            
            $.material.init();

            // Shows the modal alert if is used.
            $('#message_modal').modal('show');

            // Get the sub-sub-menu.
            $(function(){
                $('[data-submenu]').submenupicker();
            });

        });

        </script>
    </body>
    <?= $this->widget->load('wpnganalytics'); ?>
</html>
