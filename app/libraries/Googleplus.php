<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta classe fornece os métodos neessários para integração do WpanelCMS-Pro com
 * a API de autenticação do Google Plus.
 * 
 * @author Tiago Farias <tgfarias@gmail.com>
 * @since v1.0.0
 */
class Googleplus
{

    public function __construct()
    {

        $CI = & get_instance();
        $CI->config->load('googleplus');

        require APPPATH . 'third_party/google-login-api/apiClient.php';
        require APPPATH . 'third_party/google-login-api/contrib/apiOauth2Service.php';

        $this->client = new apiClient();
        $this->client->setApplicationName($CI->config->item('application_name', 'googleplus'));
        $this->client->setClientId($CI->config->item('client_id', 'googleplus'));
        $this->client->setClientSecret($CI->config->item('client_secret', 'googleplus'));
        $this->client->setRedirectUri($CI->config->item('redirect_uri', 'googleplus'));
        $this->client->setDeveloperKey($CI->config->item('api_key', 'googleplus'));
        $this->client->setScopes($CI->config->item('scopes', 'googleplus'));
        $this->client->setAccessType('online');
        $this->client->setApprovalPrompt('auto');
        $this->oauth2 = new apiOauth2Service($this->client);
    }

    /**
     * Create an Auth Url.
     * @return mixed
     */
    public function loginURL()
    {
        return $this->client->createAuthUrl();
    }

    /**
     * Authenticate an user.
     * @return mixed
     */
    public function getAuthenticate()
    {
        return $this->client->authenticate();
    }

    /**
     * Get access Token.
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->client->getAccessToken();
    }

    /**
     * Set access Token.
     * @return mixed
     */
    public function setAccessToken()
    {
        return $this->client->setAccessToken();
    }

    /**
     * Revoke token.
     * @return mixed
     */
    public function revokeToken()
    {
        return $this->client->revokeToken();
    }

    /**
     * Get user info.
     * @return mixed
     */
    public function getUserInfo()
    {
        return $this->oauth2->userinfo->get();
    }

}
