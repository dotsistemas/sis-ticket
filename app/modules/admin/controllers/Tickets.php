<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta é a classe do módulo de administração Tickets, ela foi
 * gerada automaticamente pela ferramenta Wpanel-GEN para a criação
 * de códigos padrão para o Wpanel CMS.
 *
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @since       v1.0.0
 */
class Tickets extends Authenticated_Controller
{

    /**
     * Método construtor.
     */
    function __construct()
    {
        $this->model_file = array('ticket', 'ticketmessage', 'department');
        parent::__construct();
    }

    /**
     * Mostra a lista de registros.
     */
    public function index()
    {
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        $this->table->set_heading('#', 'Departamento', 'Usuário', 'Assunto do Chamado', 'Status', 'Ações');
        // Filtra os tickets por departamento somente se for admin.
        if($this->auth->is_admin())
            $this->ticket->where('departments.account_id', $this->auth->user_id());
        $query = $this->ticket->select('tickets.*')->find_all();
        $status = config_item('status_tickets');
        foreach ($query as $row)
        {
            $this->table->add_row(
                    anchor('admin/ticketmessages/index/' . $row->id, '<span class="fa fa-ticket"></span> ' . str_pad($row->id, 5, '0', STR_PAD_LEFT)), $this->department->departament_name($row->department_id), anchor('admin/accounts/edit/' . $row->account_id, $this->nome_usuario($row->account_id)), $row->subject, $status[$row->status],
                    // Ícones de ações
                    div(array('class' => 'btn-group btn-group-xs')) .
                    anchor('admin/ticketmessages/index/' . $row->id, glyphicon('edit'), array('class' => 'btn btn-default')) .
                    '<button class="btn btn-default" onClick="return confirmar(\'' . site_url('admin/tickets/delete/' . $row->id) . '\');">' . glyphicon('trash') . '</button>' .
                    div(null, true)
            );
        }
        $this->set_var('listagem', $this->table->generate());
        $this->render();
    }

    /**
     * Retorna o nome do usuário indicado pelo Id.
     * 
     * @param int $account_id
     * @return mixed
     */
    private function nome_usuario($account_id = NULL)
    {
        if ($account_id == NULL)
            return FALSE;

        $account = $this->auth->account($account_id);
        $extra = (Object) json_decode($account->extra_data);
        return '<span class="fa fa-user"></span> ' . $extra->name;
    }

    /**
     * Encerra um chamado indicado pelo Id.
     * 
     * @param int $ticket_id
     */
    public function close($ticket_id = NULL)
    {
        if ($ticket_id == NULL)
            $this->set_message('Selecione um ticket de chamado para ser visualizado.', 'danger', 'tickets');

        $data['status'] = 2;
        if ($this->ticket->update($ticket_id, $data))
            $this->set_message('Chamado encerrado com sucesso.', 'success', 'admin/tickets');
        else
            $this->set_message('Erro ao encerrar o chamado.', 'danger', 'admin/tickets');
    }

    /**
     * Exclui um registro.
     * 
     * @param int $id Id do registro a ser excluído.
     */
    public function delete($id = NULL)
    {
        if ($id == NULL)
            $this->set_message('Registro inexistente.', 'danger', 'admin/tickets');

        if ($this->ticket->delete($id))
        {
            $this->ticketmessage->delete_by('ticket_id', $id);
            $this->set_message('Registro excluído com sucesso!', 'success', 'admin/tickets');
        } else
            $this->set_message('Erro ao excluir o registro.', 'danger', 'admin/tickets');
    }

}

// End of file modules/admin/controllers/Tickets.php