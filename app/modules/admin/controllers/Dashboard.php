<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Control panel dashboard.
 *
 * @author Eliel de Paula <dev@gelieldepaula.com.br>
 * @since v1.0.0
 * */
class Dashboard extends Authenticated_Controller
{

    /**
     * Class constructor.
     */
    function __construct()
    {
        $this->model_file = array('ticket', 'ticketmessage', 'department');
        parent::__construct();

        if (auth_login_data('role') == 'user')
            redirect('users');
    }

    /**
     * Index page.
     */
    public function index()
    {
        
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        $this->table->set_heading('#', 'Departamento', 'Usuário', 'Assunto do Chamado', 'Status');
        // Filtra os tickets por departamento somente se for admin.
        if($this->auth->is_admin())
            $this->ticket->where('departments.account_id', $this->auth->user_id());
        $query = $this->ticket->select('tickets.*')->where('tickets.status!=', 2)->find_all();
        //$query = $this->ticket->select('tickets.*')->where(array('departments.account_id'=> $this->auth->user_id(), 'tickets.status!='=>2))->find_all();
        $status = config_item('status_tickets');
        foreach ($query as $row)
        {
            $this->table->add_row(
                    anchor('admin/ticketmessages/index/' . $row->id, '<span class="fa fa-ticket"></span> ' . str_pad($row->id, 5, '0', STR_PAD_LEFT)), $this->department->departament_name($row->department_id), anchor('admin/accounts/edit/' . $row->account_id, $this->nome_usuario($row->account_id)), $row->subject, $status[$row->status]
            );
        }
        if(count($query) <= 0)
            $this->set_var('listagem', '<p class="text-danger">Nenhum chamado recente para listar.</p>');
        else
            $this->set_var('listagem', $this->table->generate());
        $this->set_var('total_aberto', $this->ticket->count_by(array('status' => 0)));
        $this->set_var('total_andamento', $this->ticket->count_by(array('status' => 1)));
        $this->set_var('total_fechado', $this->ticket->count_by(array('status' => 2)));
        $this->set_var('status', config_item('status_tickets'));

        $this->render();
    }
    
    /**
     * Retorna o nome do usuário indicado pelo Id.
     * 
     * @param int $account_id
     * @return mixed
     */
    private function nome_usuario($account_id = NULL)
    {
        if ($account_id == NULL)
            return FALSE;

        $account = $this->auth->account($account_id);
        $extra = (Object) json_decode($account->extra_data);
        return '<span class="fa fa-user"></span> ' . $extra->name;
    }

}
