<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta é a classe do módulo de administração Ticketmessages, ela foi
 * gerada automaticamente pela ferramenta Wpanel-GEN para a criação
 * de códigos padrão para o Wpanel CMS.
 *
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @version		v1.0.0
 */
class Ticketmessages extends Authenticated_Controller
{

    /**
     * Método construtor.
     */
    function __construct()
    {
        $this->model_file = array('ticket', 'ticketmessage', 'department');
        parent::__construct();
    }

    /**
     * Lista as mensagens do chamado indicado pelo Id.
     * 
     * @param int $ticket_id
     */
    public function index($ticket_id = NULL)
    {
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        $this->table->set_heading('#', 'Account_id', 'Content', 'Ações');
        $query = $this->ticketmessage->find_many_by('ticket_id', $ticket_id);
        $ticket = $this->ticket->find($ticket_id);
        $html = '';
        foreach ($query as $row)
        {
            $account = $this->auth->account($row->account_id);
            @$extra = json_decode($account->extra_data);

            @$nome = $extra->name;
            @$avatar = $extra->avatar;
            $img = array(
                'src' => 'media/avatar/' . $avatar,
                'width' => '64',
                'height' => '64'
            );

            $html .= '';
            if ($avatar)
                $html .= '    <a class="pull-left" href="#">' . img($img) . '</a>';
            else
                $html .= '<a class="pull-left" href="#"><img class="media-object" src="' . base_url('assets/img/no-user.jpg') . '" height="64" width="64"></a>';
            $html .= '    <div class="media-body" style="padding-left:5px;">';
            $html .= '      <h4 class="media-heading">' . $nome . '</h4>';
            $html .= '      <p>' . $row->content . '</p>';
            if ($row->attachment)
            {
                $html .= '      <p><a href="' . base_url('./media/anexos/' . $row->attachment) . '" target="_blank" class="btn btn-xs btn-primary"><span class="fa fa-download"></span> Anexo</a></p>';
            }
            $html .= '      <p class="text-muted">' . datetime_for_user($row->created_on) . '</p>';
            $html .= '  </div>';
        }
        if(count($query) <= 0)
            $html .= '<p class="text-danger">Nenhuma resposta para este chamado.</p>';
        $status = array(
            0 => '<span class="label label-success">Aberto</span>',
            1 => '<span class="label label-info">Em Atendimento</span>',
            2 => '<span class="label label-danger">Fechado</span>'
        );

        $account = $this->account->find($ticket->account_id);
        $extra = (object) json_decode($account->extra_data);

        $this->set_var('status', $status);
        $this->set_var('ticket', $ticket);
        $this->set_var('department', $this->department->find($ticket->department_id));
        $this->set_var('account', $account);
        $this->set_var('extra', $extra);
        $this->set_var('listagem', $html);
        $this->render();
    }

    /**
     * Adiciona uma nova resposta ao chamado de acordo com o Id.
     * 
     * @param int $ticket_id
     */
    public function add($ticket_id = NULL)
    {

        if ($ticket_id == NULL)
            $this->set_message('Registro inexistente.', 'danger', 'admin/ticketmessages');

        $this->form_validation->set_rules('content', 'Conteúdo', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->set_var('ticket_id', $ticket_id);
            $this->render();
        } else
        {
            $data = array();
            $data['ticket_id'] = $ticket_id;
            $data['account_id'] = $this->auth->user_id();
            $data['content'] = $this->input->post('content');
            $data['attachment'] = $this->wpanel->upload_media('anexos');
            if ($this->ticketmessage->insert($data))
            {
                // Recupera os dados do ticket e do dono.
                //--------------------------------------------------------------
                $query_ticket = $this->ticket->find($ticket_id);
                $account = $this->auth->account($query_ticket->account_id);
                //Enviar email
                //--------------------------------------------------------------
                $data_message = array(
                    'ticket_user' => auth_extra_data('name'),
                    'ticket_email' => auth_login_data('email'),
                    'ticket_title' => 'Nova resposta ao chamado #' . str_pad($ticket_id, 5, '0', STR_PAD_LEFT),
                    'ticket_created_on' => date('d/m/Y H:i:s'),
                    'ticket_ip' => $_SERVER['REMOTE_ADDR'],
                    'ticket_content' => $data['content']
                );
                $data_email = array(
                    'html' => TRUE,
                    'from_name' => wpn_config('site_titulo'),
                    'from_email' => wpn_config('site_contato'),
                    'to' => $this->department->notification_contacts($query_ticket->department_id) . ',' . $account->email,
                    'subject' => 'Nova resposta ao chamado #' . str_pad($ticket_id, 5, '0', STR_PAD_LEFT),
                    'message' => $this->load->view('emails/ticket_message', $data_message, TRUE),
                );
                $this->wpanel->send_email($data_email);
                //--------------------------------------------------------------
                $this->ticket->update($ticket_id, array('status' => 1));
                $this->set_message('Registro salvo com sucesso!', 'success', 'admin/ticketmessages/index/' . $ticket_id);
            } else
                $this->set_message('Erro ao salvar o registro.', 'danger', 'admin/ticketmessages/index/' . $ticket_id);
        }
    }

}
