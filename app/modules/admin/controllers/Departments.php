<?php

/**
 * WPanel CMS
 *
 * An open source Content Manager System for websites and systems using CodeIgniter.
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2008 - 2017, Eliel de Paula.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package     WpanelCms
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @copyright   Copyright (c) 2008 - 2017, Eliel de Paula. (https://elieldepaula.com.br/)
 * @license     http://opensource.org/licenses/MIT  MIT License
 * @link        https://wpanel.org
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Esta é a classe do módulo de administração Departments, ela foi
 * gerada automaticamente pela ferramenta Wpanel-GEN para a criação
 * de códigos padrão para o Wpanel CMS.
 *
 * @author      Eliel de Paula <dev@elieldepaula.com.br>
 * @version		v1.0.0
 */
class Departments extends Authenticated_Controller
{

    /**
     * Método construtor.
     */
    function __construct()
    {
        $this->model_file = array('department', 'account');
        parent::__construct();
    }

    /**
     * Mostra a lista de registros.
     */
    public function index()
    {
        $this->load->library('table');
        $this->table->set_template(array('table_open' => '<table id="grid" class="table table-striped">'));
        //TODO Revise as colunas da tabela.
        $this->table->set_heading('#', 'Title', 'Responsável', 'Ações');
        $query = $this->department->find_all();
        foreach ($query as $row)
        {
            //TODO Revise as colunas da lista.
            $this->table->add_row(
                    $row->id, $row->title, anchor('admin/accounts/edit/'.$row->account_id,$this->nome_usuario($row->account_id)),
                    // Ícones de ações
                    div(array('class' => 'btn-group btn-group-xs')) .
                    anchor('admin/departments/edit/' . $row->id, glyphicon('edit'), array('class' => 'btn btn-default')) .
                    '<button class="btn btn-default" onClick="return confirmar(\'' . site_url('admin/departments/delete/' . $row->id) . '\');">' . glyphicon('trash') . '</button>' .
                    div(null, true)
            );
        }
        $this->set_var('listagem', $this->table->generate());
        $this->render();
    }

    /**
     * Mostra o formulário e cadastra um novo registro.
     */
    public function add()
    {
        $this->form_validation->set_rules('title', 'Título', 'required');
        $this->form_validation->set_rules('account_id', 'Responsável', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->set_var('accounts', $this->account->where('role !=', 'user')->find_all());
            $this->render();
        } else
        {
            $data = array();
            $data['account_id'] = $this->input->post('account_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['manager'] = $this->input->post('manager');
            $data['email'] = $this->input->post('email');
            $data['status'] = $this->input->post('status');

            if ($this->department->insert($data))
                $this->set_message('Registro salvo com sucesso!', 'success', 'admin/departments');
            else
                $this->set_message('Erro ao salvar o registro.', 'danger', 'admin/departments');
        }
    }

    /**
     * Mostra o formulário e altera um registro.
     * 
     * @param int $id Id do registro a ser editado.
     */
    public function edit($id = NULL)
    {
        $this->form_validation->set_rules('title', 'Título', 'required');
        $this->form_validation->set_rules('account_id', 'Responsável', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            if ($id == NULL)
                $this->set_message('Registro inexistente.', 'danger', 'admin/departments');
            $this->set_var('accounts', $this->account->where('role !=', 'user')->find_all());
            $this->set_var('row', $this->department->find($id));
            $this->render();
        } else
        {
            $data = array();
            $data['account_id'] = $this->input->post('account_id');
            $data['title'] = $this->input->post('title');
            $data['description'] = $this->input->post('description');
            $data['manager'] = $this->input->post('manager');
            $data['email'] = $this->input->post('email');
            $data['status'] = $this->input->post('status');

            if ($this->department->update($id, $data))
                $this->set_message('Registro salvo com sucesso!', 'success', 'admin/departments');
            else
                $this->set_message('Erro ao salvar o registro.', 'danger', 'admin/departments');
        }
    }

    /**
     * Exclui um registro.
     * 
     * @param int $id Id do registro a ser excluído.
     */
    public function delete($id = NULL)
    {
        if ($id == NULL)
            $this->set_message('Registro inexistente.', 'danger', 'admin/departments');

        if ($this->department->delete($id))
            $this->set_message('Registro excluído com sucesso!', 'success', 'admin/departments');
        else
            $this->set_message('Erro ao excluir o registro.', 'danger', 'admin/departments');
    }
    
    /**
     * Retorna o nome do usuário indicado pelo Id.
     * 
     * @param int $account_id
     * @return mixed
     */
    private function nome_usuario($account_id = NULL)
    {
        if ($account_id == NULL)
            return FALSE;

        $account = $this->auth->account($account_id);
        $extra = (Object) json_decode($account->extra_data);
        return '<span class="fa fa-user"></span> ' . $extra->name;
    }

}

// End of file modules/admin/controllers/Departments.php