<?php
// Captura o avatar do usuário.
$avatar = auth_extra_data('avatar');
if ($avatar)
    $avatar = base_url('media/avatar') . '/' . $avatar;
else
    $avatar = base_url('lib/img') . '/no-user.jpg';
?>
<section class="content-header">
    <h1>
        <?php echo wpn_lang('mod_dashboard', 'Dashboard'); ?>
        <small><?php echo wpn_lang('wpn_welcome', 'Welcome to WPanel CMS'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-dashboard"></i> <?php echo wpn_lang('mod_dashboard', 'Dashboard'); ?></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-2 col-sm-3 hidden-xs"><img src="<?php echo $avatar; ?>" class="img-responsive img-thumbnail" alt="<?php echo auth_extra_data('name'); ?>"/></div>
                <div class="col-md-4">
                    <h2><?php echo wpn_lang('wpn_hello', 'Hello'); ?> <?php echo auth_extra_data('name'); ?>.</h2>
                    <p><?php echo auth_extra_data('name'); ?> - <?php echo auth_login_data('email'); ?></p>
                    <p><?php echo wpn_lang('wpn_since', 'Since'); ?> <?php echo datetime_for_user(auth_login_data('created_on'), 0); ?></p>
                    <p><?php echo anchor('admin/accounts/profile', wpn_lang('lnk_manageprofile', 'Manage my profile'), array('class' => 'btn btn-primary')); ?></p>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-5">
                    <h4><?php echo wpn_lang('wpn_sumary', 'Sumary'); ?> geral dos chamados</h4>
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td><?php echo $status[0]; ?></td>
                                <td><?php echo badge($total_aberto); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $status[1]; ?></td>
                                <td><?php echo badge($total_andamento); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo $status[2]; ?></td>
                                <td><?php echo badge($total_fechado); ?></td>
                            </tr>
                            <tr>
                                <td><span class="label label-default">Total de Chamados</span></td>
                                <td><?php echo badge($total_aberto + $total_andamento + $total_fechado); ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <section class="content">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Chamados</h3>
                        </div>
                        <div class="box-body">
                            <?php echo $listagem; ?>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>