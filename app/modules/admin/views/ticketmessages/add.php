<section class="content-header">
    <h1>
        Respostas dos Chamados
        <small>Módulo gerenciador de chamados.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?= site_url('admin/ticketmessages/index/' . $ticket_id); ?>"><i class="fa fa-cog"></i> Responder chamado</a></li>
        <li>Cadastro</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Formulário de cadastro</h3>
        </div>
        <div class="box-body">
            <?= form_open_multipart('admin/ticketmessages/add/' . $ticket_id, array('class' => 'form-horizontal')); ?>
                <div class="form-group">
                    <label for="department_id" class="col-sm-2 control-label">Mensagem</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="content" placeholder="Texto do seu chamado"rows="5"></textarea>
                        <?= form_error('content'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="department_id" class="col-sm-2 control-label">Anexar um arquivo</label>
                    <div class="col-sm-10">
                        <input type="file" name="userfile" class="form-control">
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10">
                        <button type="submit" class="btn btn-primary" >Salvar</button>
                        <?= anchor('admin/ticketmessages/index/' . $ticket_id, 'Cancelar', array('class' => 'btn btn-danger')); ?>
                    </div> 
                </div>
            <?= form_close(); ?>
        </div>
    </div>
</section>