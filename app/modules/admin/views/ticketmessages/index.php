<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<section class="content-header">
    <h1>
        Respostas do ticket
        <small>Módulo gerenciador de tickets de chamados.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?= site_url('admin/tickets'); ?>"><i class="fa fa-cog"></i> Tickets</a></li>
        <li><?= anchor('admin/ticketmessages/add/' . $ticket->id, glyphicon('plus-sign') . 'Responder chamado'); ?></li>
    </ol>
</section>
<section class="content-header">
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-md-12">
            <h2><span class="label label-info"><?= str_pad($ticket->id, 5, '0', STR_PAD_LEFT); ?></span> <?= $ticket->subject; ?> <?php echo anchor('admin/tickets', '<span class="fa fa-caret-left"></span>  Voltar', array('class' => 'btn btn-primary pull-right')); ?></h2>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-4 col-md-4">
            <b>Data:</b>
            <?= datetime_for_user($ticket->created_on); ?>
        </div>
        <div class="col-sm-4 col-md-4">
            <b>Status:</b>
            <?= $status[$ticket->status]; ?>
        </div>
        <div class="col-sm-4 col-md-4">
            <b>Responsável:</b>
            <?= auth_extra_data('name'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4 col-md-4">
            <b>Aberto por:</b>
            <?= $extra->name; ?>
        </div>
        <div class="col-sm-4 col-md-4">
            <b>E-mail:</b>
            <?= $account->email ?>
        </div>
        <div class="col-sm-4 col-md-4">
            <b>Departamento:</b>
            <?= $department->title ?>
        </div>
    </div>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-4 col-md-4">
            <?php if($ticket->attachment){ ?><a href="<?= base_url('./media/anexos/'.$ticket->attachment); ?>" target="_blank" class="btn btn-primary btn-xs"><span class="fa fa-download"></span> Baixar anexo</a><?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box" style="margin-bottom: 0px; padding-bottom: 0px;">
                <div class="box-header">
                    <h3 class="box-title">Mensagem do chamado</h3>
                </div>
                <div class="box-body">
                    <p><?= $ticket->content; ?></p>
                </div>
            </div>
        </div>
    </div>   
</section>
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Respostas</h3>
            <div class="box-tools pull-right hidden-xs">
                <?= anchor('admin/tickets/close/' . $ticket->id, glyphicon('remove-sign') . 'Encerrar chamado', array('class' => 'btn btn-danger')); ?>
                <?= anchor('admin/ticketmessages/add/' . $ticket->id, glyphicon('plus-sign') . 'Responder chamado', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
        <div class="box-body">
            <?= $listagem; ?>
        </div>
        <div class="box-footer">            
            <div class="box-tools pull-right">
                <?= anchor('admin/tickets/close/' . $ticket->id, glyphicon('remove-sign') . 'Encerrar chamado', array('class' => 'btn btn-danger')); ?>
                <?= anchor('admin/ticketmessages/add/' . $ticket->id, glyphicon('plus-sign') . 'Responder chamado', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>
</section>