<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<section class="content-header">
    <h1>
        Tickets de chamado
        <small>Módulo gerenciador de tickets.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><i class="fa fa-cog"></i> Tickets de chamado</li>
    </ol>
</section>

<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Listagem</h3>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <?= $listagem; ?>
            </div>
        </div>
    </div>
</section>