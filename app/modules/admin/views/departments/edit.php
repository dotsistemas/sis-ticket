<section class="content-header">
    <h1>
        Departamentos
        <small>Módulo gerenciador de departamentos.</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('admin/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?= site_url('admin/departments'); ?>"><i class="fa fa-cog"></i> Departamentos</a></li>
        <li>Alteração</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Formulário de alteração</h3>
        </div>
        <div class="box-body">
            <form action="<?= site_url('admin/departments/edit/' . $row->id); ?>" role="form" class="form-horizontal" method="post" accept-charset="utf-8">

                <div class="form-group">
                    <label for="id" class="col-sm-2 col-md-2 control-label">Título</label>
                    <div class="col-sm-10 col-md-10">
                        <input type="text" name="title" id="title" value="<?= $row->title; ?>" class="form-control" />
                        <?= form_error('title'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="id" class="col-sm-2 col-md-2 control-label">Descrição</label>
                    <div class="col-sm-10 col-md-10">
                        <textarea name="description" id="description" class="form-control" rows="5"><?= $row->description; ?></textarea>
                        <?= form_error('description'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="id" class="col-sm-2 col-md-2 control-label">Responsável</label>
                    <div class="col-sm-4 col-md-4">
                        <select name="account_id" id="account_id" class="form-control">
                            <?php
                            foreach($accounts as $acc){
                                $extra = (Object) json_decode($acc->extra_data);
                                ?>
                                <option value="<?php echo $acc->id; ?>" <?php if($acc->id == $row->account_id) { echo 'selected'; } ?>><?php echo $extra->name; ?></option>
                            <?php } ?>
                        </select>
                        <?= form_error('account_id'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="id" class="col-sm-2 col-md-2 control-label">Notificação (opcional)</label>
                    <div class="col-sm-4 col-md-4">
                        <input type="text" name="email" id="email" value="<?= $row->email; ?>" class="form-control" />
                        <i>Separe com vírgula caso haja mais de um e-mail.</i><br/>
                        <?= form_error('email'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="id" class="col-sm-2 col-md-2 control-label">Status</label>
                    <div class="col-sm-3 col-md-3">
                        <select name="status" id="status" class="form-control">
                            <option value="1" <?php if($row->status == 1){ echo 'selected'; } ?> >Ativo</option>
                            <option value="0" <?php if($row->status == 0){ echo 'selected'; } ?> >Inativo</option>
                        </select>
                        <?= form_error('status'); ?>
                    </div>
                </div>

                <hr/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 col-md-offset-2 col-md-10">
                        <button type="submit" class="btn btn-primary" >Salvar</button>
                        <?= anchor('admin/departments', 'Cancelar', array('class' => 'btn btn-danger')); ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>