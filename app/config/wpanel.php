<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Available languages.
 */
$config['available_languages'] = array(
    'english'    => array('locale' => 'en', 'label' => 'Inglês'), 
    'portuguese' => array('locale' => 'pt_BR', 'label' => 'Português')
);

/**
 * Available web-editors.
 */
$config['available_editors'] = array('ckeditor'=>'CKEditor', 'tinymce'=>'TinyMCE');

/**
 * Define os tipos de usuário serão permitidos no site. 
 */
//$config['users_role'] = array('user' => 'Usuário comum', 'admin' => 'Administrador');

/**
 * Available views for posts lists.
 */
$config['posts_views'] = array('list' => 'Listagem', 'mosaic' => 'Mosaico');

/**
 * Available banners positions.
 */
$config['banner_positions'] = array('slide' => 'Slide-Show', 'sidebar' => 'Barra lateral');

/**
 * Available functional links for menu management.
 */
$config['funcional_links'] = array(
    'home'      => 'Página inicial',
    'contact'   => 'Página de contato',
    //'galleries' => 'Galeria de fotos',
    //'videos'    => 'Galeria de videos',
    //'events'    => 'Lista de eventos',
    //'pool'      => 'Lista de enquetes',
    'users'     => 'Área do usuário',
    'rss'       => 'Página de RSS',
);

/**
 * Status for tickets.
 */
$config['status_tickets'] = array(
    0 => '<span class="label label-success">Aberto</span>',
    1 => '<span class="label label-info">Em Atendimento</span>',
    2 => '<span class="label label-danger">Fechado</span>'
);