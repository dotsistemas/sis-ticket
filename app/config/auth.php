<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Allowed user types.
 */
$config['auth_account_role'] = array('ROOT'=>'Super-User', 'user' => 'Usuário comum', 'admin' => 'Administrador');

/**
 * Check permissions into a hook for all methods (global).
 */
$config['auth_check_permyssion_by_hook'] = FALSE;

/**
 * Register a log from user access.
 */
$config['auth_log_access'] = TRUE;

/**
 * Enable the bann IP's.
 */
$config['auth_enable_ip_banned'] = TRUE;

/**
 * Max login attempts.
 */
$config['auth_max_attempts'] = 10;

/**
 * Automatic ban for IP that exceeded the limit of login attempts.
 */
$config['auth_enable_autoban'] = TRUE;

/**
 * Type of hash for passwords.
 */
$config['auth_password_hash_type'] = 'md5';

/**
 * Password salt for hash.
 */
$config['auth_password_hash_salt'] = '';

/**
 * Set an white-list to free access into some links.
 */
$config['auth_white_list'] = array(
    'admin',
    'admin/login',
    'admin/dashboard',
    'admin/accounts/profile',
    /*

    // Gerador WpanelCMS
    'gerador',
    'gerador/main',
    'gerador/main/gerarModel',
    'gerador/main/gerarController',
    'gerador/main/gerarCrud'

    */
);
