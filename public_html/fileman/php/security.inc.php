<?php
/*
  RoxyFileman - web based file manager. Ready to use with CKEditor, TinyMCE.
  Can be easily integrated with any other WYSIWYG editor or CMS.

  Copyright (C) 2013, RoxyFileman.com - Lyubomir Arsov. All rights reserved.
  For licensing, see LICENSE.txt or http://RoxyFileman.com/license

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  Contact: Lyubomir Arsov, liubo (at) web-lobby.com
*/

/**
 * Check security to fileman.
 */
function checkAccess($action)
{
    if (!is_logged())
        exit('No direct script access allowed');
}

/**
 * This function read the session file to check if the ser is logged and then
 * decide if open or not the file manager.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 * @since v1.0.0
 * @return boolean
 */
function is_logged()
{
    $output = false;
    $filename = sys_get_temp_dir() . '/wpanel_' . $_COOKIE['wpanel_'];
    if (file_exists($filename))
    {
        $handle = fopen($filename, "r");
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        $x = explode(';', $contents);
        foreach ($x as $key => $value)
        {
            $y = explode('|', $value);
            if (verify($y)){
                $output = true;
                break;
            } else
                $output = false;
        }
    }
    return $output;
}

/**
 * Verify the logged_in in the array exploded.
 *
 * @author Eliel de Paula <dev@elieldepaula.com.br>
 * @since v1.0.0
 * @return boolean
 */
function verify($y)
{
    if ($y[0] == 'logged_in'){
        return true;
        break;
    } else
        return false;
}
