#ATENÇÃO!

Este projeto foi transferido para o GitHub sob o endereço https://github.com/dotsistemas/sys-ticket 

Entre em contato com elieldepaula@gmail.com para conseguir acesso ao repositório.

---

#Sys-Ticket

##Um gerenciador de tickets de chamado baseado no Wpanel CMS Pro 1.1.0.

O Sys-Ticket é uma solução para gerenciamento de tickets de chamado desenvolvida com PHP usando o CMS WpanelCMS-Pro na versão 1.1.0, que por sua vez é desenvolvido usando o Framework CodeIgniter.

O sistema funciona com qualquer um dos bancos de dados suportados pelo CodeIgniter, porém só foi testado com MySQL e SQLite 3.

###Requisitos

- PHP >= 5.6.*
- MySql ou Sqlite
- Biblioteca Mcrypt ativada.

###Instalação

1. Faça o download do arquivo do projeto em .zip/.tar.gz, ou você pode clonar o repositório;
2. Certifique-se de dar permissão de escrita nas pastas: app/cache, app/db, public_html/captcha e public_html/media;
3. Altere o arquivo public_html/index.php de acordo com seu ambiente e projeto;
4. Altere o arquivo public_html/config/database.php de acordo com o banco de dados que deseja usar;
	Caso deseje usar MySql, você precisará configurar uma nova base de dados no seu servidor.
5. Acesse seu site pelo navegador, no primeiro acesso você será direcionado para a criação de um usuário administrador;
6. Faça seu login no painel de controle, clique na opção 'Visualizar Site', se tudo ocorreu bem seu Sys-Ticket já está configurado :)

#The MIT License (MIT)

Copyright (c) 2014 Eliel de Paula
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.